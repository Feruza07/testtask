<?php

$user = 'root';
$password = '';
$db = 'contact';
$host = '127.0.0.1';
$charset = 'utf8';
try {
    $pdo = new \PDO("mysql:host=$host;dbname=$db;charset=$charset", $user, $password);
} catch (Exception $e) {
    print $e->getMessage() . "\n";
}
