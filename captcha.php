<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
session_start();
$captcha_num = rand(1000, 9999);
$_SESSION['code'] = $captcha_num;
$font_size = 30;
$img_width = 100;
$img_height = 40;
header('Content-type: image/jpeg');
$image = imagecreate($img_width, $img_height);
imagecolorallocate($image, 255, 255, 255);
$text_color = imagecolorallocate($image, 0, 0, 0);
imagettftext($image, $font_size, 0, 15, 30, $text_color, __DIR__.'/Pacifico.ttf', $captcha_num);
imagejpeg($image);
?>