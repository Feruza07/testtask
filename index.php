<?php
require('vendor/autoload.php');
require('db.php');

use Rakit\Validation\Validator;

session_start();


$captchaError = false;
$successMessage = false;
$errors = [];

if (isset($_POST) & !empty($_POST)) {

    if ($_POST['captcha'] != $_SESSION['code']) {
        $captchaError = "incorrect captcha";
    }

    $validator = new Validator;

    $validation = $validator->make($_POST + $_FILES, [
        'name' => 'required',
        'email' => 'required|email',
        'text' => 'required|min:6',
    ]);

    $validation->validate();

    if ($validation->fails()) {
        $errors = $validation->errors();
        $errors = $errors->firstOfAll();
        if ($captchaError) {
            $errors['captcha'] = $captchaError;
        }

    } else {
        $request = $_POST;
        $date = strtotime(date('Y-m-d H:i:s'));
        $name = $request['name'];
        $sql = 'INSERT INTO info (name, email, text, created_at) VALUES (:name, :email, :text, :created_at)';
        $query = $pdo->prepare($sql);
        $arr = ['name' => $name, 'email' => $request['email'], 'text' => $request['text'], 'created_at' => $date];
        if ($query->execute($arr)) {
            $successMessage = "Спасибо, {$name}";
        }
    }
}


?>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-auto">
            <div class="card mt-5">
                <div class="card-header text-center">
                    <h2>contact form</h2>
                </div>
                <div class="card-body">
                    <?php if($successMessage):?>
                    <div class="alert alert-success">
                        <?=$successMessage?>
                    </div>
                    <?php else:?>
                    <?php if(!empty($errors)):?>
                    <div class="alert alert-danger">
                        <ul>
                            <?php foreach($errors as $name => $error): ?>
                            <li><?="{$name} - {$error}"?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <?php endif;?>
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="name">name</label>
                            <input type="text" name="name" placeholder="Enter name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="email">email</label>
                            <input type="email" name="email" placeholder="Enter email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="text"></label>
                            <textarea name="text" id="" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <img src="captcha.php" />
                        </div>
                        <div class="form-group">
                            <label for="captcha">code</label>
                            <input type="text" name="captcha" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="submit" value="send" class="btn btn-success">
                        </div>
                    </form>
                    <?php endif;?>
                </div>
            </div>

        </div>
    </div>
</div>

</body>
</html>